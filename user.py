from utils import add_years_name


class User:
    def __init__(self, name='', age=0, phone='', email='', info='',
                 vkontakte='', facebook='', instagram='', telegram='', tiktok='', ogrnip='', inn=''):
        # user profile
        self.name = name
        self.age = age
        self.phone = phone
        self.email = email
        self.info = info
        self.ogrnip = ogrnip
        self.inn = inn
        # social links
        self.vkontakte = vkontakte
        self.facebook = facebook
        self.instagram = instagram
        self.telegram = telegram
        self.tiktok = tiktok


    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def to_dict(self):
        return {
            'name': self.name,
            'age': self.age,
            'phone': self.phone,
            'email': self.email,
            'info': self.info,
            'ogrnip': self.ogrnip,
            'inn': self.inn,
            # social links
            'vkontakte': self.vkontakte,
            'facebook': self.facebook,
            'instagram': self.instagram,
            'telegram': self.telegram,
            'tiktok': self.tiktok,
        }

    def input_general_info(self):
        name = input('Введите имя: ')
        while True:
            # validate user age
            try:
                age = int(input('Введите возраст: '))
            except ValueError:
                age = 0
            if age > 0:
                break
            print('Возраст должен быть положительным')
        phone = input('Введите номер телефона: ')
        email = input('Введите адрес электронной почты: ')
        ogrnip = input('Введите ОГРНИП: ')
        inn = input('Введите ИНН: ')
        info = input('Введите дополнительную информацию:\n')

        self.update(name=name, age=age, phone=phone, email=email, info=info, ogrnip=ogrnip, inn=inn)

    def input_social_links(self):
        vkontakte = input('Введите адрес профиля Вконтакте: ')
        facebook = input('Введите адрес профиля Facebook: ')
        instagram = input('Введите адрес профиля Instagram: ')
        telegram = input('Введите логин Telegram: ')
        tiktok = input('Введите логин Tiktok: ')
        self.update(vkontakte=vkontakte, facebook=facebook,
                    instagram=instagram, telegram=telegram,
                    tiktok=tiktok)

    def print_info(self, full=False):
        print('Имя:    ', self.name)
        print('Возраст:', add_years_name(self.age))
        print('Телефон:', self.phone)
        print('E-mail: ', self.email)
        if self.info:
            print('')
            print('Дополнительная информация:')
            print(self.info)

        if full:
            # print social links
            print('')
            print('Социальные сети и мессенджеры')
            print('Вконтакте:', self.vkontakte)
            print('Facebook: ', self.facebook)
            print('Instagram:', self.instagram)
            print('Telegram: ', self.telegram)
            print('Tiktok:   ', self.tiktok)
            print('ОГРНИП:   ', self.ogrnip)
            print('ИНН:      ', self.inn)